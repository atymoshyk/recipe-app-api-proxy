# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Env vars

* `LISTEN_PORT` - port to listen on (default: '8000')
* `APP_HOST` - Hostname of the app to forward requests to (default: 'app')
* `APP_PORT` - port of the app